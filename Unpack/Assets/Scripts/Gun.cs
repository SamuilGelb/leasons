﻿using System;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public int _ammoInHolder = 30;//Oboyma
    public int _maxAmmoInHolder = 30;//Max v oboyme
    public int _ammoInBag = 90;//v inventore

    public Rigidbody _bulletPrefab;
    public float _bulletSpeed = 10;

    public bool Reload()
    {
        if (_ammoInHolder != _maxAmmoInHolder)
        {
            _ammoInBag += _ammoInHolder;
            _ammoInHolder = 0;
            if (_ammoInBag > 0)
            {
                if (_ammoInBag >= _maxAmmoInHolder)
                {
                    _ammoInHolder = _maxAmmoInHolder;
                    _ammoInBag -= _maxAmmoInHolder;
                    return true;
                }
                else
                {
                    _ammoInHolder = _ammoInBag;
                    _ammoInBag = 0;
                    return true;
                }
            }
        }
        return false;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            if (Reload())
            {
                Debug.Log("Animation perezaryadka");
                Debug.Log("UpdateInterface");
            }
            else
            {
                Debug.Log("Ti debil");
            }
        }
        if(Input.GetMouseButton(0))
        {
            if(Shoot())
            {

            }
        }
    }

    private bool Shoot()
    {
        if(_ammoInHolder > 0)
        {
            var bullet = Instantiate(_bulletPrefab, transform.position, Quaternion.identity);
            bullet.velocity = transform.forward * _bulletSpeed;
            Destroy(bullet.gameObject, 10f);
            _ammoInHolder--;
            return true;
        }
        return false;
    }

    private void OnGUI()
    {
        GUI.Box(new Rect(Screen.width / 2 - 5, Screen.height / 2 - 5, 10, 10), "");
    }
}
