﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BrockenWall : MonoBehaviour
{
    public void Init()
    {
        var rgbds = GetComponentsInChildren<Rigidbody>().ToList();
        int c = rgbds.Count;
        for (int i = 0; i < c; i++)
        {
            float rndX = Random.Range(-20f, 20f);
            float rndY = Random.Range(-20f, 20f);
            float rndZ = Random.Range(-20f, 20f);
            rgbds[i].velocity = new Vector3(rndX, rndY, rndZ);
        }
        Destroy(gameObject, 10f);
    }
}
