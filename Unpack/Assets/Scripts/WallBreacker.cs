﻿using UnityEngine;

public class WallBreacker : MonoBehaviour
{
    public BrockenWall _brokenPrefab;

    public void Broke()
    {
        var brk = Instantiate(_brokenPrefab, transform.position, transform.rotation);
        brk.Init();
        float rndX = Random.Range(-40f, 40f);
        float rndZ = Random.Range(-40f, 40f);
        Instantiate(gameObject, new Vector3(rndX, 1.8f, rndZ), transform.rotation);
        Destroy(gameObject);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Bullet")
        {
            Broke();
        }
    }
}
