﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOnElevator : MonoBehaviour
{
    public Animator _elevator;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !_elevator.enabled)
        {
            _elevator.enabled = true;
        }
    }
}
