﻿using UnityEngine;

public class FlashLight : MonoBehaviour
{
    public Light _lighter = default;
    public Rigidbody col;

    void Start()
    {
        _lighter.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            //_lighter.enabled = !_lighter.enabled;
            //col.velocity = Vector3.up * 2;
            col.velocity = new Vector3(1, 4, -2);
        }
    }
}
