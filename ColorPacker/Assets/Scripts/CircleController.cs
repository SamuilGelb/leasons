﻿using UnityEngine;
using UnityEngine.UI;

public class CircleController : MonoBehaviour
{
    public Image _circleImage;
    public CircleColor _color;
    public VesselController _vessel;

    internal void Init(CircleColor circleColor, VesselController controller)
    {
        _color = circleColor;
        _vessel = controller;
        gameObject.name = $"Circle_{circleColor.ToString("f")}";
        switch (circleColor)
        {
            case CircleColor.Red:
                _circleImage.color = Color.red;
                break;
            case CircleColor.Green:
                _circleImage.color = Color.green;
                break;
            case CircleColor.Blue:
                _circleImage.color = Color.blue;
                break;
            case CircleColor.Yellow:
                _circleImage.color = Color.yellow;
                break;
            case CircleColor.Cyan:
                _circleImage.color = Color.cyan;
                break;
            case CircleColor.Magenta:
                _circleImage.color = Color.magenta;
                break;
            case CircleColor.White:
                break;
            case CircleColor.Grey:
                break;
            case CircleColor.Brown:
                break;
            case CircleColor.DarkGreen:
                break;
            case CircleColor.Purple:
                break;
            case CircleColor.Black:
                break;
            case CircleColor.DarkRed:
                break;
            case CircleColor.Pink:
                break;
            default:
                break;
        }
    }
}
