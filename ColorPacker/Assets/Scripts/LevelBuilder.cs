﻿using System.Collections.Generic;
using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
    public VesselController _vesselPref;
    public RectTransform _gameArea;
    public List<VesselController> _vessels = new List<VesselController>();

    public void Build(DataModel data)
    {
        int c = data.Vessel.Count;
        for (int i = 0; i < c; i++)
        {
            var vessel = Instantiate(_vesselPref, _gameArea);
            vessel.Init(data.Vessel[i]);
            _vessels.Add(vessel);
        }
    }
}
