﻿using System.IO;
using UnityEngine;

public static class DataModelGetter
{
    public static DataModel GetLevel(int num)
    {
        string path = Path.Combine(Application.persistentDataPath, $"level_{num}.dat");
        //string sa = string.Format("level_{0}{1}{2}{3}{4}{5}{6}.dat", num);
        DataModel data = new DataModel();
        if (File.Exists(path))
        {
            string text = File.ReadAllText(path);
            data = JsonUtility.FromJson<DataModel>(text);
            return data;
        }
        else
        {
            return data;
        }
    }

    public static void SaveLevel(int num, DataModel model)
    {
        string path = Path.Combine(Application.persistentDataPath, $"level_{num}.dat");
        string data = JsonUtility.ToJson(model);
        StreamWriter sw = File.CreateText(path);
        sw.WriteLine(data);
        sw.Close();
    }
}