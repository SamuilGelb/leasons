﻿using System;
using System.Collections.Generic;

[Serializable]
public class DataModel
{
    public int LevelNum;
    public List<Vessel> Vessel;
    public Reward Reward;

    public DataModel()
    {
        Vessel = new List<Vessel>();
        Reward = new Reward();
    }
}

[Serializable]
public class Reward
{
    public int Money;
    public int Reverce;
    public int Hint;
    public string Skin;
}

[Serializable]
public class Vessel
{
    public List<CircleColor> Circles;

    public Vessel()
    {
        Circles = new List<CircleColor>();
    }
}

public enum CircleColor
{
    Red = 0,
    Green = 1,
    Blue = 2,
    Yellow = 3,
    Cyan = 4,
    Magenta = 5,
    White = 6,
    Grey = 7,
    Brown = 8,
    DarkGreen = 9,
    Purple = 10,
    Black = 11,
    DarkRed = 12,
    Pink = 13
}