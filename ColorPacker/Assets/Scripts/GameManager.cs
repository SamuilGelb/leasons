﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public CircleController _currentSelected;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void OnVesselClick(VesselController ctrl)
    {
        if(_currentSelected == null)
        {
            _currentSelected = ctrl._circles[ctrl._circles.Count - 1];
            return;
        }
        else
        {
            if(ctrl.TryPut(_currentSelected))
            {
                _currentSelected = null;
            }
            else
            {
                _currentSelected = ctrl._circles[ctrl._circles.Count - 1];
            }
        }
    }
}
