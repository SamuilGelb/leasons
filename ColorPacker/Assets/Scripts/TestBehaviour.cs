﻿using UnityEngine;

public class TestBehaviour : MonoBehaviour
{
    public DataModel levelData;
    public LevelBuilder _levelBuilder;

    public void Awake()
    {
        //DataModelGetter.SaveLevel(1, levelData);
        levelData = DataModelGetter.GetLevel(1);
        _levelBuilder.Build(levelData);
    }
}
