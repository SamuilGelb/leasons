﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VesselController : MonoBehaviour
{
    public CircleController _circlePrefab;
    public List<CircleController> _circles = new List<CircleController>();
    public const int MAX_CIRCLES = 4;
    public Button _button;

    public void Awake()
    {
        if (_button == null)
            _button = GetComponent<Button>();
        _button.onClick.AddListener(() => { OnVesselClick(); });
    }

    private void OnVesselClick()
    {
        GameManager.Instance.OnVesselClick(this);
    }

    internal bool TryPut(CircleController currentSelected)
    {
        if (_circles.Count >= MAX_CIRCLES)
        {
            return false;
        }
        else
        {
            if (_circles.Count == 0 
                || _circles[_circles.Count - 1]._color == currentSelected._color)
            {
                _circles.Add(currentSelected);
                currentSelected.transform.SetParent(transform);
                currentSelected.transform.SetSiblingIndex(0);
                currentSelected._vessel._circles.Remove(currentSelected);
                currentSelected._vessel = this;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public void Init(Vessel data)
    {
        gameObject.name = $"Vessel {Random.Range(0, 20)}";  
        int c = data.Circles.Count;
        c = Mathf.Min(c, MAX_CIRCLES);
        for (int i = 0; i < c; i++)
        {
            var circle = Instantiate(_circlePrefab, Vector3.zero, Quaternion.identity);
            circle.transform.SetParent(transform);
            circle.Init(data.Circles[i], this);
            _circles.Add(circle);
        }
        _circles.Reverse();
    }
}
